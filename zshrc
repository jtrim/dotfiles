export ZSH=$HOME/.oh-my-zsh
export ZSH_THEME="cookrn"

set -o AUTO_CD

oss=~/Projects/lib/oss
web=~/Projects/lib/web
desk=~/Desktop
adam=~web/adamfwatkins3

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Example format: plugins=(rails git textmate ruby lighthouse)
plugins=(git gem)

EDITOR=/usr/bin/vim
alias rspec='nocorrect rspec'

source $ZSH/oh-my-zsh.sh

source "$HOME/.includes/inits"
source "$HOME/.includes/paths"
source "$HOME/.includes/functions"
source "$HOME/.includes/aliases"
if [ -e "$HOME/.includes/foraker" ]; then
  source "$HOME/.includes/foraker"
fi
source "$HOME/.includes/Littlenote/littlenote.sh"
export EDITOR=vim

if [[ -e ~/.zshrc.local ]]; then
  source ~/.zshrc.local
fi
